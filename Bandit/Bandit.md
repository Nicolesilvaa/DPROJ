![logo InfoJr Dark](https://media-exp1.licdn.com/dms/image/C560BAQFb_8HjpS-0vw/company-logo_100_100/0/1519892133780?e=2147483647&v=beta&t=WmFiZHDMCc1nW_5IO8-oVJC93lXNeOvo432fMaLHdH0)

# InfoJr UFBA - DPROJ  

*Trainee Nicole Silva*

*28/05/2022*

# Atividade  Bandit

### Senha encontrada no nível 0

    boJ9jbbUNNfktd78OOpsqOltutMc3MY1

comandos usados:

- ls
- cat readme

***

### Senha encontrada no nível 1

    CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9

comandos usados:

```PowerShell
- ls
- cat ./-
```

***

### Senha encontrada no nível 2

    UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK

comandos usados:

```PowerShell
- ls
- cat + tab
```

***

### Senha encontrada no nível 3

    pIwrPrtPN36QITSp3EQaw936yaFoFgAB

comandos usados:

```PowerShell
- ls
- cd inhare/
- ls
- cat + tab -> saída: cat.hidden
```

### Senha encontrada no nível 4

    koReBOKuIDDepwhWk7jZC0RTdopnAYKh

comandos usados:

```PowerShell
- ls
- cd inhare/
- ls
- file -f -file00 (e assim sucessivamente até achar a senha no -file07)
```

***

### Senha encontrada no nível 5

    DXjZPULLxYr17uwoI01bNLQbtFemEgo7

comandos usados:

```PowerShell
- ls
- cd inhare/
- ls
- file -f -file00 (e assim sucessivamente até achar a senha no -file07)
```

***

### Senha encontrada no nível 6

    HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs

comandos usados:

```PowerShell
- ls
- find -size 1033c
- cd ./maybehere07 & cat .file2
```

***

### Senha  encontrada no nível 7

    cvX2JJa4CFALtqS87jk27qwqGhBM9plV

comandos usados:

```PowerShell
- ls
- cd ..
- ls 
- find -size 33c
- cat /etc/group
- find /home -uid 300 | tee 300-files.txt
- find / -user bandit7
- find / -user bandit7 -group bandit6 -size 33c -readable
- cat /var/lib/dpkg/info/bandit7.password 
```

***

### Senha encontrada no nível 8

    UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR

comandos usados:

```PowerShell
- ls
- grep millionth data.txt
```

***

### Senha encontrada no nível 9

    truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk

comandos usados:

```PowerShell
- ls
- cat data.txt | sort
- cat data.txt | sort | uniq -u
```

***

### Senha encontrada no nível 10

    IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR

Comandos usados:

```PowerShell
- ls
- cat data.txt & grep "="
- cat data.txt | base64 -d
```

***

### Senha encontrada no nível 11

    5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu

Comandos usados:

```PowerShell
- ls
- cat data.txt | rot13 -d
- echo "Gur cnffjbeq vf 5Gr8L4qetPEsPk8htqjhRK8XSP6x2RHh" |tr  ' [A-Za-z] '  ' [N-ZA-Mn-za-m] '
```

***

### Senha encontrada no nível 12

    .

```PowerShell
Comandos usados:

- ls
- mkdir /tmp/HackerGirl
- mv
- cd /tmp/HackerGirl
- mv data.txt data.hex
- xxd -b data.hex
- hexdump -c senha.txt
```

***
